﻿namespace App.Shared
{
    public class FileUpload
    {
        public Guid Id { get; set; }
        public string FileName { get; set; } = string.Empty;
        public string ContentType { get; set; } = string.Empty;
        public byte[] FileContent { get; set; } = new byte[1];//set default value to avoid null exception
    }
}
