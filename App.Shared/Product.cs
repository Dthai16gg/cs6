﻿using System;
using System.ComponentModel.DataAnnotations;

namespace App.Shared
{
    public class Product
    {
        [Key]
        public Guid IdGuid { get; set; }
        [Required]
        [StringLength(50,ErrorMessage = "Name isn't long 50 character")]
        public string Name { get; set; } = string.Empty;
        [Required]
        [MaxLength(250,ErrorMessage = "Description isn't long 250 character")]
        public string Description { get; set; } = string.Empty;
        [Required]
        [Range(0,1000000,ErrorMessage = "Price must be between 0 and 1000000")]
        public decimal Price { get; set; }
    }
}