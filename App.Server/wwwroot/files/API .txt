API :

1. Program.cs
builder.Services.AddDbContext<ApplicationDbContext>(options => options.UseSqlite(builder.Configuration.GetConnectionString("DefaultConnectionString")));
//add dependency injection
builder.Services.AddScoped<IUploadFileService, UploadFileService>();
builder.Services.AddScoped<IProductService, ProductService>();
builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(builder =>
    {
        builder.WithOrigins("https://localhost:7224") // Replace with your Blazor client's URL
            .AllowAnyHeader()
            .AllowAnyMethod();
    });
});
2. Upload file service
//check file is empty
            if (file == null || file.Length == 0)
            {
                return "File is not selected";
            }
//file done is txt jpg xls
            var ext = Path.GetExtension(file.FileName);
            var validExt = new[] { ".txt", ".jpg", ".xls" };
            if (!validExt.Contains(ext))
            {
                return "Invalid File";
            }
try
            {
                // add file to wwwroot/files
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/files", file.FileName);
                await using var stream = new FileStream(path, FileMode.Create);
                await file.CopyToAsync(stream);
                //convert file to byte
                await using var dataStream = new MemoryStream();
                await file.CopyToAsync(dataStream);
                var fileBytes = dataStream.ToArray();
                //save file to db
                var fileUpload = new FileUpload
                {
                    Id = Guid.NewGuid(), 
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileContent = fileBytes
                };
                await _db.UploadedFiles.AddAsync(fileUpload);
                await _db.SaveChangesAsync();
                return "File Uploaded Successfully";
            }
            catch (Exception e)
            {
               return e.Message;
            }
3. upload file controller
[HttpPost("Upload-File")]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            return Ok(await _uploadFileService.UploadFile(file));
        }
4.Product controller
[HttpDelete("Delete/{idGuid:guid}")]
        public async Task<IActionResult> Delete(Guid idGuid)
        {
            return Ok(await _productService.DeleteProduct(idGuid));
        }
[HttpGet("Get/{idGuid:guid}")]
        public async Task<IActionResult> Get(Guid idGuid)
        {
            return Ok(await _productService.GetProduct(idGuid));
        }
Blazor
1. Product service
- get all : return await _client.GetFromJsonAsync<IEnumerable<Product>>("api/Product/Get-All");
- get one :  var response = await _client.GetAsync($"api/Product/Get/{id}");
                if (!response.IsSuccessStatusCode) return null;
                var content = await response.Content.ReadAsStringAsync();
                var product = JsonConvert.DeserializeObject<Product>(content);
- create :  var response = await _client.PostAsJsonAsync("api/Product/Add", product);
                response.EnsureSuccessStatusCode(); // Throw if not success
                var content = await response.Content.ReadAsStringAsync();
                var createdProduct = JsonConvert.DeserializeObject<Product>(content);
- update : 
		 var response = await _client.PutAsJsonAsync("api/Product/Update", product);
                response.EnsureSuccessStatusCode(); // Throw if not success
                var content = await response.Content.ReadAsStringAsync();
                var updatedProduct = JsonConvert.DeserializeObject<Product>(content);
- delete :  var response = await _client.DeleteAsync($"api/Product/Delete/{idGuid}");
                response.EnsureSuccessStatusCode(); // Throws if not successful
2. Blazor component :
- show : 
[Inject]
    private NavigationManager NavigationManager { get; set; }

    [Inject]
    private IProductService ProductService { get; set; }

    private IEnumerable<Product> Products { get; set; } = new List<Product>();

    private int count { get; set; } = 1;

    protected override async Task OnInitializedAsync()
    {
        Products = await ProductService.GetAllAsync();
    }

    private async Task DeleteProduct(Guid id)
    {
        await ProductService.DeleteAsync(id);
        NavigationManager.NavigateTo("/index", true);
    }

    private async Task EditProduct(Guid id)
    {
        NavigationManager.NavigateTo($"/updateProduct/{id}", true);
    }
- update :
	[Inject]
    private NavigationManager NavigationManager { get; set; }

    [Inject]
    private IProductService ProductService { get; set; }

    [Parameter]
    public Guid Id { get; set; }

    private Product Product { get; set; } = new Product();

    private string PriceString
    {
        get => Product.Price.ToString();
        set => Product.Price = decimal.Parse(value);
    }

    protected override async Task OnInitializedAsync()
    {
        Product = await ProductService.GetAsync(Id);
    }

    private async Task HandleValidSubmit()
    {
        if (Product != null)
        {
            await ProductService.UpdateAsync(Product);
            NavigationManager.NavigateTo("/index", true);
        }
    }
- add : 
   [Inject]
    private NavigationManager NavigationManager { get; set; }
    [Inject]
    private IProductService ProductService { get; set; }

    private Product Product { get; set; } = new Product();

    private async Task HandleValidSubmit()
    {
        if (Product == null)
        {
            NavigationManager.NavigateTo("/", true);
        }
        await ProductService.CreateAsync(Product);
        NavigationManager.NavigateTo("/index", true);
    }
    [Required]
    [Range(0, 100000, ErrorMessage = " Price must be between 0 and 100000")]
    private string PriceString
    {
        get => Product.Price.ToString();
        set => Product.Price = decimal.TryParse(value, out var parsedPrice) ? parsedPrice : 0;
    }
   		
- upload file  :
private IBrowserFile file { get; set; }

    private string uploadMessage { get; set; } = string.Empty;

    [Inject]
    public HttpClient httpClient { get; set; }

    private async Task HandleFileChange(InputFileChangeEventArgs e)
    {
        file = e.File;
        uploadMessage = string.Empty;
    }

    private async Task UploadFile()
    {
        var content = new MultipartFormDataContent();
        var streamContent = new StreamContent(file.OpenReadStream(file.Size));

        streamContent.Headers.ContentType = new MediaTypeHeaderValue(file.ContentType);

        content.Add(streamContent, "file", file.Name);

        var response = await httpClient.PostAsync("api/File/Upload-File", content);

        uploadMessage = response.IsSuccessStatusCode ? "Successfully." : "Failed.";
    }
