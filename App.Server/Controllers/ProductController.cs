﻿using App.Server.Service.IService;
using App.Shared;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        // GET: api/<ProductController>
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet("Get-All")]
        public async Task<IActionResult> GetAll()
        {
           return Ok(await _productService.GetProducts());
        }

        // GET api/<ProductController>/5
        [HttpGet("Get/{idGuid:guid}")]
        public async Task<IActionResult> Get(Guid idGuid)
        {
            return Ok(await _productService.GetProduct(idGuid));
        }

        // POST api/<ProductController>
        [HttpPost("Add")]
        public async Task<IActionResult> Post([FromBody] Product product)
        {
            return Ok(await _productService.AddProduct(product));
        }
        // PUT api/<ProductController>/5
        [HttpPut("Update")]
        public async Task<IActionResult> Put([FromBody] Product product)
        {
            return Ok(await _productService.UpdateProduct(product));
        }
        // DELETE api/<ProductController>/5
        [HttpDelete("Delete/{idGuid:guid}")]
        public async Task<IActionResult> Delete(Guid idGuid)
        {
            return Ok(await _productService.DeleteProduct(idGuid));
        }
    }
}
