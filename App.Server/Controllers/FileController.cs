﻿using App.Server.Service.IService;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private readonly IUploadFileService _uploadFileService;

        public FileController(IUploadFileService uploadFileService)
        {
            _uploadFileService = uploadFileService;
        }
        //get all
        [HttpGet("Get-All")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _uploadFileService.GetAllFiles());
        }
        //add file
        [HttpPost("Upload-File")]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            return Ok(await _uploadFileService.UploadFile(file));
        }
    }
}
