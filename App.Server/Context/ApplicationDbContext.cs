﻿using App.Shared;
using Microsoft.EntityFrameworkCore;

namespace App.Server.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<FileUpload> UploadedFiles { get; set; }
        public DbSet<Product> Products { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //map the model to a table
            modelBuilder.Entity<FileUpload>().ToTable("FileUpload");
            modelBuilder.Entity<Product>().ToTable("Product");

        }
    }
}
