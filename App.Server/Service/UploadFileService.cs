﻿using App.Server.Context;
using App.Server.Service.IService;
using App.Shared;
using Microsoft.EntityFrameworkCore;

namespace App.Server.Service
{
    public class UploadFileService : IUploadFileService
    {
        private readonly ApplicationDbContext _db; //inject db context

        public UploadFileService(ApplicationDbContext db)
        {
            _db = db;
        }
        public async Task<string> UploadFile(IFormFile? file)
        {
            //check file is null
            //check file is empty
            if (file == null || file.Length == 0)
            {
                return "File is not selected";
            }
            //file done is txt jpg xls
            var ext = Path.GetExtension(file.FileName);
            var validExt = new[] { ".txt", ".jpg", ".xls" };
            if (!validExt.Contains(ext))
            {
                return "Invalid File";
            }
            try
            {
                // add file to wwwroot/files
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/files", file.FileName);
                await using var stream = new FileStream(path, FileMode.Create);
                await file.CopyToAsync(stream);
                //convert file to byte
                await using var dataStream = new MemoryStream();
                await file.CopyToAsync(dataStream);
                var fileBytes = dataStream.ToArray();
                //save file to db
                var fileUpload = new FileUpload
                {
                    Id = Guid.NewGuid(),
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileContent = fileBytes
                };
                await _db.UploadedFiles.AddAsync(fileUpload);
                await _db.SaveChangesAsync();
                return "File Uploaded Successfully";
            }
            catch (Exception e)
            {
               return e.Message;
            }
        }

        public async Task<IEnumerable<FileUpload>> GetAllFiles()
        {
            //don't get file content because it's too large
            return (await _db.UploadedFiles.ToListAsync()).Select(file => new FileUpload { Id = file.Id, FileName = file.FileName, ContentType = file.ContentType }).ToList();
        }
    }
}
