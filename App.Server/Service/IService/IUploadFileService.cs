﻿using App.Shared;

namespace App.Server.Service.IService
{
    public interface IUploadFileService
    {
        Task<string> UploadFile(IFormFile? file);
        Task<IEnumerable<FileUpload>> GetAllFiles();
    }
}
