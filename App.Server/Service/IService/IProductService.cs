﻿using App.Shared;

namespace App.Server.Service.IService
{
    public interface IProductService
    {
        //crud
        Task<IEnumerable<Product>> GetProducts();
        Task<Product> GetProduct(Guid id);
        Task<string> AddProduct(Product product);
        Task<string> UpdateProduct(Product product);
        Task<string> DeleteProduct(Guid id);
    }
}
