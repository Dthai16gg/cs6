﻿using App.Server.Context;
using App.Server.Service.IService;
using App.Shared;
using Microsoft.EntityFrameworkCore;

namespace App.Server.Service
{
    public class ProductService : IProductService
    {
        private readonly ApplicationDbContext _db;

        public ProductService(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<IEnumerable<Product>> GetProducts()
        {
            try
            {
                return await _db.Products.ToListAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<Product> GetProduct(Guid id)
        {
            try
            {
                return await _db.Products.FindAsync(id) ?? throw new Exception("Don't Get Product");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<string> AddProduct(Product product)
        {
            try
            {
                if (product == null) throw new Exception("Product is null");
                await _db.Products.AddAsync(product);
                await _db.SaveChangesAsync();
                return "Product Created Successfully";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public async Task<string> UpdateProduct(Product product)
        {
            try
            {
                if (product == null) throw new Exception("Product is null");
                _db.Products.Update(product);
                await _db.SaveChangesAsync();
                return "Product Created Successfully";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public async Task<string> DeleteProduct(Guid id)
        {
            try
            {
                var product = await _db.Products.FindAsync(id);
                if (product == null) throw new Exception("Product is null");
                _db.Products.Remove(product);
                await _db.SaveChangesAsync();
                return "Product Created Successfully";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}
