﻿
using System.Net.Http.Json;
using App.Shared;
using Client.Service.IService;
using Newtonsoft.Json;

namespace Client.Service
{
    public class ProductService : IProductService
    {
        private readonly HttpClient _client;

        public ProductService(HttpClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<IEnumerable<Product>> GetAllAsync()
        {
            try
            {
                return await _client.GetFromJsonAsync<IEnumerable<Product>>("api/Product/Get-All");
            }
            catch (HttpRequestException)
            {
                // Handle or log the exception as needed
                return null;
            }
        }

        public async Task<Product?> GetAsync(Guid id)
        {
            try
            {
                var response = await _client.GetAsync($"api/Product/Get/{id}");
                if (!response.IsSuccessStatusCode) return null;
                var content = await response.Content.ReadAsStringAsync();
                var product = JsonConvert.DeserializeObject<Product>(content);
                return product;
            }
            catch (HttpRequestException)
            {
                // Handle or log the exception as needed
                return null;
            }
        }

        public async Task<Product> CreateAsync(Product product)
        {
            try
            {
                var response = await _client.PostAsJsonAsync("api/Product/Add", product);
                response.EnsureSuccessStatusCode(); // Throw if not success
                var content = await response.Content.ReadAsStringAsync();
                var createdProduct = JsonConvert.DeserializeObject<Product>(content);
                return createdProduct;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public async Task<Product> UpdateAsync(Product product)
        {
            try
            {
                var response = await _client.PutAsJsonAsync("api/Product/Update", product);
                response.EnsureSuccessStatusCode(); // Throw if not success
                var content = await response.Content.ReadAsStringAsync();
                var updatedProduct = JsonConvert.DeserializeObject<Product>(content);
                return updatedProduct;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public async Task DeleteAsync(Guid idGuid)
        {
            try
            {
                var response = await _client.DeleteAsync($"api/Product/Delete/{idGuid}");
                response.EnsureSuccessStatusCode(); // Throws if not successful
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
